import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tournament_flutter/map/map_page.dart';

class LoginController extends Controller {
  FirebaseAuth auth = FirebaseAuth.instance;
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController smsCodeController = TextEditingController();

  String verificationId = '';
  bool askForCode = false;

  @override
  void initListeners() {
    auth.authStateChanges().listen((User? user) {
      if (user == null) {
        Fluttertoast.showToast(msg: 'User is currently signed out!');
      } else {
        Navigator.push(
            getContext(), MaterialPageRoute(builder: (context) => MapPage()));
      }
    });
  }

  void connectUser() {
    String phoneNumber = phoneNumberController.text;

    auth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        verificationCompleted: (PhoneAuthCredential credential) async {
          Fluttertoast.showToast(msg: 'Verification completed');
        },
        verificationFailed: (FirebaseAuthException e) {
          Fluttertoast.showToast(msg: 'Verification failed');
        },
        codeSent: (String verificationId, int? resendToken) async {
          askForCode = true;
          this.verificationId = verificationId;
          refreshUI();
        },
        codeAutoRetrievalTimeout: (String verificationId) {});
  }

  void checkCode() {
    String smsCode = smsCodeController.text;
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: verificationId, smsCode: smsCode);
    auth.signInWithCredential(credential);
  }
}