import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';

import 'login_controller.dart';

class LoginPage extends View {
  @override
  State<StatefulWidget> createState() => LoginState();
}

class LoginState extends ViewState<LoginPage, LoginController> {
  LoginState() : super(LoginController());

  @override
  Widget get view => MaterialApp(
        title: 'Tournament Flutter',
        theme: ThemeData(
          scaffoldBackgroundColor: const Color(0xFFFADEB6),
          fontFamily: 'NatureRough',
        ),
        home: Scaffold(
          key: globalKey,
          body: ControlledWidgetBuilder<LoginController>(
              builder: (context, controller) {
            return Center(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.fromLTRB(32.0, 128.0, 32.0, 64.0),
                    child: Text(
                      'Tournament \n\nFlutter',
                      style: TextStyle(
                        fontSize: 48.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Boorsok',
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text(
                      !controller.askForCode
                          ? 'N° de téléphone'
                          : 'Code de vérification',
                      style: TextStyle(
                        fontSize: 32.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(64.0, 4.0, 64.0, 32.0),
                    child: TextField(
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        hintText:
                            !controller.askForCode ? '+33XXXXXXXXX' : 'XXXXXX',
                      ),
                      style: TextStyle(
                        fontSize: 24.0,
                      ),
                      controller: !controller.askForCode
                          ? controller.phoneNumberController
                          : controller.smsCodeController,
                    ),
                  ),
                  Stack(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(32.0, 0.0, 32.0, 0.0),
                        child: TextButton(
                          onPressed: !controller.askForCode
                              ? controller.connectUser
                              : controller.checkCode,
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16.0),
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Text(
                                !controller.askForCode
                                    ? 'SE CONNECTER'
                                    : 'VERIFIER',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24.0,
                                ),
                              ),
                            ),
                            color: const Color(0xFFE67B1E),
                          ),
                        ),
                      ),
                      Positioned(
                        right: 0,
                        bottom: 0,
                        top: 0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16.0),
                              ),
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(10.0, 4.0, 10.0, 4.0),
                                child: Text('i'),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            );
          }),
        ),
      );
}
