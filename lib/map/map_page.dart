import 'package:flutter/widgets.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

import 'map_controller.dart';

class MapPage extends View {
  @override
  State<StatefulWidget> createState() => MapState();
}

class MapState extends ViewState<MapPage, MapController> {
  MapState() : super(MapController());

  @override
  Widget get view => MapboxMap(
        accessToken:
            "sk.eyJ1IjoidGNvcXVhbiIsImEiOiJja3FvdzEybW0waHNqMnlxaGo2N3k2OWo5In0.hCu0TwtM5Q3YwzxqF9ayvQ",
        initialCameraPosition: CameraPosition(
          zoom: 15.0,
          target: LatLng(45.795, 4.816),
        ),
      );
}
